﻿using System.Security.Cryptography;
using System.Text;

namespace OrinocoAccess
{
    public static class Hash
    {
        /// <summary>
        /// Will hash the given string.
        /// </summary>
        /// <param name="value">A string.</param>
        /// <returns>Returns hash as string.</returns>
        public static string ToHash(string value)
        {
            const string FORMAT_HEX = "x2";

            SHA512 hasher = SHA512.Create();
            StringBuilder stringBuilder = new StringBuilder();

            // Convert the input string to a byte array
            byte[] bytesValue = hasher.ComputeHash(Encoding.UTF8.GetBytes(value));

            // Compute the hash.
            foreach (byte valueByte in bytesValue)
                stringBuilder.Append(valueByte.ToString(FORMAT_HEX).ToUpper());

            return stringBuilder.ToString();
        }
    }
}
