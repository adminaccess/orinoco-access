﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace OrinocoAccess
{
    [Flags]
    public enum AccessLevel
    {
        None = 0x0,
        Read = 0x1,
        Write = 0x2,
        Create = 0x4,
        Delete = 0x8
    };
    
    public enum Department : uint
    {
        None,
        Server,
        Admin,
        Payroll,
        Accounts,
        Purchasing,
        Calendar,
        Invoicing,
        OurBay,
        Holiday,
        RoomBooking,
        Stock,
        DigitalDocs,
        InternalComms,
        ProjManSystem
    };    

    public class DepartmentAccess
    {
        public Department department;
        public AccessLevel accessLevel;

        // Default constructor
        public DepartmentAccess()
        {
            department = new Department();
            accessLevel = new AccessLevel();
        }

        /// <summary>
        /// Constructor with two parameters
        /// </summary>
        /// <param name="inDept">The department of type Department.</param>
        /// <param name="inAccessLevel">The accesslevel(s) of the department of type AccessLevel.</param>
        public DepartmentAccess(Department inDept, AccessLevel inAccessLevel)
        {
            department = inDept;
            accessLevel = inAccessLevel;
        }

        // Destructor
        ~DepartmentAccess()
        {
            department = Department.None;
            accessLevel = AccessLevel.None;
        }        
    }
}
