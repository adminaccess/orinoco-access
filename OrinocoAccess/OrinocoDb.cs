﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace OrinocoAccess
{
    public class OrinocoDB
    {
        private const int NO_OF_ARGUMENTS = 3;
        private const int ARG_USER_ID = 1;
        private const int ARG_AUTH_STR = 2;

        // Points to the path of the database file
        private string mDbFile;

        public List<OrinocoUser> Users;

        // Default constructor
        public OrinocoDB(string dbFile)
        {
            mDbFile = dbFile;
            Users = new List<OrinocoUser>();
        }

        // Destructor
        ~OrinocoDB()
        {
            mDbFile = "";
            Users.Clear();
        }

        /// <summary>
        /// Get the path of the current database file.
        /// </summary>
        /// <returns>A string with the file path.</returns>
        public string GetDatabasePath()
        {
            return mDbFile;
        }

        /// <summary>
        /// Saves the list of users to the given database.
        /// </summary>
        public void Save()
        {
            XmlSerializer writer = new XmlSerializer(typeof(List<OrinocoUser>));
            FileStream dbFile = null;

            try
            {
                dbFile = File.Create(mDbFile);
                writer.Serialize(dbFile, Users);
            }
            catch (Exception ex)
            {
                if (dbFile != null)
                    dbFile.Close();
                throw new Exception(ex.Message);
            }

            dbFile.Close();
        }

        /// <summary>
        /// Loads the given database file into the Users list.
        /// </summary>
        public void Load()
        {
            XmlSerializer reader = new XmlSerializer(typeof(List<OrinocoUser>));
            StreamReader dbFile = null;

            try
            {
                dbFile = new StreamReader(mDbFile);
                Users = (List<OrinocoUser>)reader.Deserialize(dbFile);
            }
            catch (Exception ex)
            {
                if (dbFile != null)
                    dbFile.Close();
                throw new Exception(ex.Message);
            }

            dbFile.Close();
        }

        /// <summary>
        /// Clears the the current database in memory.
        /// </summary>
        public void Close()
        {
            Users.Clear();
        }

        /// <summary>
        /// Get a user object by employee ID.
        /// </summary>
        /// <param name="employeeID">The employee ID</param>
        /// <returns>OrinocoUser object</returns>
        public OrinocoUser GetUserById(uint employeeID)
        {
            foreach (OrinocoUser user in Users)
            {
                if (user.EmployeeID == employeeID)
                {
                    return user;
                }
            }

            // The user was not fount, so return null
            return null;
        }

        /// <summary>
        /// Get a user object by the user's name
        /// </summary>
        /// <param name="username">The username of the user.</param>
        /// <returns>OrinocoUser object.</returns>
        public OrinocoUser GetUserByName(string username)
        {
            foreach (OrinocoUser user in Users)
            {
                if (user.Username == username)
                {
                    return user;
                }
            }

            // The user was not found, so return null
            return null;
        }

        /// <summary>
        /// Using the arguments passed to the program, the function
        /// will check if the user is authorised.
        /// If authorised, will return the user object.
        /// </summary>
        /// <returns>Returns validated OrinocoUser. Returns null if error or not authenticated.</returns>
        public OrinocoUser GetAuthorisedUser()
        {
            string[] args = Environment.GetCommandLineArgs();
            uint userId = 0;
            string authStr = "";
            OrinocoUser user;

            // Check if there were the correct number of arguments passed
            if (args.Count<string>() == NO_OF_ARGUMENTS)
            {
                // Get user id and authentication string
                userId = uint.Parse(args[ARG_USER_ID]);
                authStr = args[ARG_AUTH_STR];

                // Load user by ID
                user = GetUserById(userId);

                // Check if user loaded
                if (user != null)
                {
                    if (authStr == user.GetAuthString()) // Check if authenticated
                        return user;
                    else
                        return null;
                }
                else
                    return null;
            }
            return null;
        }
    }
}
