﻿using System;
using System.Collections.Generic;

namespace OrinocoAccess
{
    public class OrinocoUser
    {
        private const int DEPARTMENT_LIST_FULL = -1;
        private const int DEPARTMENT_NOT_FOUND = -1;
        private const char DELIMINATOR = ':';

        private uint mEmployeeID;
        private string mUsername;
        private string mPassword;

        public List<DepartmentAccess> Departments;

        // Default constructor
        public OrinocoUser()
        {
            mEmployeeID = 0;
            mUsername = "";
            mPassword = "";
            Departments = new List<DepartmentAccess>();
        }

        // Destructor
        ~OrinocoUser()
        {
            mEmployeeID = 0;
            mUsername = "";
            mPassword = "";
            Departments.Clear();
        }

        // EmployeeID property
        public uint EmployeeID
        {
            get { return mEmployeeID; }
            set { mEmployeeID = value; }
        }

        // Username property
        public string Username
        {
            get { return mUsername; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Username cannot be empty or null!");

                mUsername = value;
            }
        }

        // Password property
        public string Password
        {
            get { return mPassword; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Password cannot be empty or null!");

                mPassword = value;
            }
        }

        /// <summary>
        /// Get the index of a given department in the Departments list.
        /// </summary>
        /// <param name="dept">Type Department</param>
        /// <returns>Integer value of the index.</returns>
        private int GetDeptIndex(Department dept)
        {
            int i = 0;
            bool foundDept = false;

            while (foundDept == false && i < Departments.Count)
            {
                if (Departments[i].department == dept)
                    foundDept = true;
                else
                    i++;
            }

            if (foundDept == false)
                return DEPARTMENT_NOT_FOUND;

            return i;
        }

        /// <summary>
        /// Add the given Department with access level to the Departments list.
        /// Use this method to add departments. Do NOT use Departments.Add() as it could cause duplicates!
        /// Deparments has to be public due to serialization.
        /// </summary>
        /// <param name="deptAccess">DepartmentAccess object.</param>
        /// Example:
        ///     DepartmentAccess deptAccess = new DepartmentAccess();
        ///     deptAccess.department = Department.Payroll;
        ///     deptAccess.accessLevel = AccessLevel.Read | AccessLevel.Write;
        ///     AddDepartment(deptAccess);
        public void AddDepartment(DepartmentAccess deptAccess)
        {
            if (GetDeptIndex(deptAccess.department) != DEPARTMENT_NOT_FOUND)
                throw new Exception("User already has access to the department: " + deptAccess.department.ToString());

            Departments.Add(deptAccess);
        }

        /// <summary>
        /// Remove the given department from the Departments list.
        /// </summary>
        /// <param name="dept">Type Department</param>
        /// Example:
        ///     RemoveDepartment(Department.Holiday);
        public void RemoveDepartment(Department dept)
        {
            int deptIndex = GetDeptIndex(dept);

            if (deptIndex == DEPARTMENT_NOT_FOUND)
                return;

            Departments.RemoveAt(deptIndex);
        }

        /// <summary>
        /// Get the AccessLevel for the given department.
        /// </summary>
        /// <param name="dept">Type Department</param>
        /// <returns>Type AccessLevel</returns>
        /// Example:
        ///     AccessLevel accountsAL = GetDeptAccessLevel(Department.Accounts);
        ///     if ((accountsAL & AccessLevel.Read) != AccessLevel.None)
        ///         // Has read access
        public AccessLevel GetDeptAccessLevel(Department dept)
        {
            int deptIndex = GetDeptIndex(dept);

            if (deptIndex == DEPARTMENT_NOT_FOUND)
                return AccessLevel.None;

            return Departments[deptIndex].accessLevel;
        }

        /// <summary>
        /// Get the AccessLevel using the given index.
        /// </summary>
        /// <param name="index">Integer value of the index.</param>
        /// <returns>AccessLevel of the department.</returns>
        /// Example:
        ///     AccessLevel accountsAL = GetDeptAccessLevel(lstDepartments.SelectedIndex);
        ///     if ((accountsAL & AccessLevel.Write) != AccessLevel.None)
        ///         // Has write access
        public AccessLevel GetDeptAccessLevel(int index)
        {
            if (index >= Departments.Count)
                return AccessLevel.None;

            return Departments[index].accessLevel;
        }

        /// <summary>
        /// Check if the user has certain access levels for the given department.
        /// </summary>
        /// <param name="dept">Department</param>
        /// <param name="accessLevel">Access level(s) to test for.</param>
        /// <returns></returns>
        public bool HasDeptAccess(Department dept, AccessLevel accessLevel)
        {
            if ((this.GetDeptAccessLevel(dept) & accessLevel) == accessLevel)
                return true;

            return false;
        }

        /// <summary>
        /// Modify the given department's access level.
        /// </summary>
        /// <param name="deptAccess">DepartmentAccess object</param>
        /// Example:
        ///     DepartmentAccess newDeptAccess = new DepartmentAccess();
        ///     newDeptAccess.department = Department.Accounts;
        ///     newDeptAccess.accessLevel = AccessLevel.Read | AccessLevel.Write | AccessLevel.Create;
        ///     ModifyDeptAccess(newDeptAccess);
        public void ModifyDeptAccess(DepartmentAccess deptAccess)
        {
            int deptIndex = GetDeptIndex(deptAccess.department);

            if (deptIndex == DEPARTMENT_NOT_FOUND)
                throw new Exception("Cannot modify department access. Department " + deptAccess.department.ToString() + " was not found!");

            Departments[deptIndex] = deptAccess;
        }

        /// <summary>
        /// Generates the authentication string that is passed
        /// to other department programs.
        /// </summary>
        /// <returns></returns>
        public string GetAuthString()
        {
            return Hash.ToHash(mUsername + mPassword);
        }
    }
}
